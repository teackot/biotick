#include "Field.h"

Field::Field(int m, int n) {
    this->field = new Object **[m];
    for (int i = 0; i < m; i++) {
        this->field[i] = new Object *[n];
        for (int j = 0; j < n; j++) {
            this->field[i][j] = nullptr;
        }
    }

    this->x_size = m;
    this->y_size = n;
}

Field::~Field() {
    for (int i = 0; i < x_size; i++) {
        for (int j = 0; j < y_size; j++) {
            if (field[i][j]) delete field[i][j];
        }
        delete[] field[i];
    }
    delete[] field;
}

int Field::get_x_size() const {
    return x_size;
}

int Field::get_y_size() const {
    return y_size;
}

Object *&Field::at(int x, int y) {
    if (x < 0) {
        x = x_size + x;
    } else if (x >= x_size) {
        x = x - x_size;
    }

    if (y < 0) {
        y = y_size + y;
    } else if (y >= y_size) {
        y = y - y_size;
    }

    return field[x][y];
}
