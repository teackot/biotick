cmake_minimum_required(VERSION 3.24)
project(biotick)

set(CMAKE_CXX_STANDARD 20)

# Fetch FTXUI

#include(FetchContent)
#
#FetchContent_Declare(ftxui
#  GIT_REPOSITORY https://github.com/ArthurSonzogni/ftxui
#  GIT_TAG v3.0.0
#)
#
#FetchContent_GetProperties(ftxui)
#if(NOT ftxui_POPULATED)
#  FetchContent_Populate(ftxui)
#  add_subdirectory(${ftxui_SOURCE_DIR} ${ftxui_BINARY_DIR} EXCLUDE_FROM_ALL)
#endif()

add_executable(biotick main.cpp Simulation.cpp Simulation.h objects/Object.cpp objects/Object.h objects/Wall.cpp objects/Wall.h Action.h objects/Crab.cpp objects/Crab.h Field.cpp Field.h objects/biological/Cell.cpp objects/biological/Cell.h)

target_link_libraries(biotick ncurses)

#target_link_libraries(biotick
#  PRIVATE ftxui::screen
#  PRIVATE ftxui::dom
#  PRIVATE ftxui::component
#)
