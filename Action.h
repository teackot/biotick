#pragma once

namespace action {
    enum ActionType {
        NOP = 0,
        MOVE,
        HIT,
        HIT_CONSUME,
        SPAWN,
        EXCHANGE_DATA,
    };
}

using action::ActionType;

struct Action {
    ActionType type = action::NOP;
    int x_rel = 0, y_rel = 0;
};
