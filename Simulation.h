#pragma once

#include <vector>
using std::vector;

#include "Field.h"

#include "objects/Object.h"
using obj::Object;

class Simulation {
private:
    Field field;

    bool turn_flag;

public:
    Simulation(int m, int n);
    ~Simulation();

    void tick();

    bool spawn(int x, int y, Object *object);

    [[nodiscard]] int x_size() const;
    [[nodiscard]] int y_size() const;

    [[nodiscard]] vector<vector<char>> as_char_array();
};
