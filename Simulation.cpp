#include "Simulation.h"

#include <stdexcept>

Simulation::Simulation(int m, int n)
    : field(m, n) {
    this->turn_flag = false;
}

Simulation::~Simulation() = default;

void Simulation::tick() {
    for (int i = 0; i < x_size(); i++) {
        for (int j = 0; j < y_size(); j++) {
            auto &source = field.at(i, j);
            if (source) {
                if (source->get_turn_flag() == this->turn_flag)
                    continue;

                auto action = source->tick();
                source->set_turn_flag(this->turn_flag);

                int tx = i + action.x_rel,
                    ty = j + action.y_rel;

                switch (action.type) {
                    case action::NOP: {
                        // do nothing
                        break;
                    }
                    case action::MOVE: {
                        auto &target = field.at(tx, ty);
                        if (not target) {
                            target = source;
                            source = nullptr;
                        } else {
                            target->respond_to_action({ // TODO: maybe move out of switch
                                action.type,
                                -action.x_rel,
                                -action.y_rel
                            });
                        }

                        break;
                    }
                    case action::HIT: {
                        auto &target = field.at(tx, ty);
                        if (target) {
                            bool submitted = target->respond_to_action({ // TODO: maybe move out of switch
                                    action.type,
                                    -action.x_rel,
                                    -action.y_rel
                            });

                            // replace
                            if (submitted) {
                                auto new_object = target->get_object_to_spawn();
                                delete target;
                                target = new_object;
                            }
                        }

                        break;
                    }
                    case action::HIT_CONSUME: {
                        // TODO: consume

                        auto &target = field.at(tx, ty);
                        if (target) {
                            target->respond_to_action({ // TODO: maybe move out of switch
                                    action.type,
                                    -action.x_rel,
                                    -action.y_rel
                            });
                        }

                        break;
                    }
                    case action::SPAWN: {
                        auto &target = field.at(tx, ty);
                        if (not target) { // empty; can spawn
                            target = source->get_object_to_spawn();
                        } else { // not empty
                            if (action.x_rel == 0 and action.y_rel == 0) { // replace the actor
                                auto new_object = source->get_object_to_spawn();
                                delete source;
                                target = new_object;
                            } else { // can't spawn; notify the target
                                target->respond_to_action({ // TODO: maybe move out of switch
                                        action.type,
                                        -action.x_rel,
                                        -action.y_rel
                                });

                                /*
                                 * FIXME: this is a temporary fix to prevent a memory leak
                                 * FIXME: until action_success is implemented
                                 */
                                delete source->get_object_to_spawn();
                            }
                        }

                        break;
                    }
                    case action::EXCHANGE_DATA: {
                        // TODO: implement data exchange
                        break;
                    }
                }
            }
        }
    }

    turn_flag = not turn_flag;
}

bool Simulation::spawn(int x, int y, Object *object) {
    if (x < 0 || x > x_size()) {
        throw std::out_of_range("x coordinate is out of range");
    }

    if (y < 0 || y > y_size()) {
        throw std::out_of_range("y coordinate is out of range");
    }

    auto &target = field.at(x, y);

    if (target) {
        return false;
    }

    target = object;
    object->set_turn_flag(not this->turn_flag);
    return true;
}

int Simulation::x_size() const {
    return field.get_x_size();
}

int Simulation::y_size() const {
    return field.get_y_size();
}

vector<vector<char>> Simulation::as_char_array() {
    vector<vector<char>> arr(x_size());
    for (int i = 0; i < x_size(); i++) {
        arr[i] = vector<char>(y_size());
        for (int j = 0; j < y_size(); j++) {
            auto &object = field.at(i, j);
            arr[i][j] = object ? object->as_char() : '.';
        }
    }

    return arr;
}
