#include <iostream>

#include <ncurses.h>

#include "Simulation.h"

#include "objects/Wall.h"
#include "objects/Crab.h"
#include "objects/biological/Cell.h"

int main() {
    initscr();

    Simulation sim(10, 10);

    for (int x = 4; x <= 5; x++) {
        for (int y = 4; y <= 5; y++) {
            if (not sim.spawn(x, y, new obj::Wall())) {
                return -1;
            }
        }
    }

//    const unsigned char dna = {0};
//    sim.spawn(0, 7 - 3, new obj::bio::Cell(&dna, 1));

    sim.spawn(0, 7 - 3, new obj::Crab(1, 0));

    do {
        sim.tick();

        auto arr = sim.as_char_array();
        for (int i = 0; i < sim.x_size(); i++) {
            for (int j = 0; j < sim.y_size(); j++) {
                auto c = arr[i][j];
                mvaddch(j, i, arr[i][j]);
            }
        }

        refresh();
    } while (getch() != 'q');

    endwin();
    return 0;
}
