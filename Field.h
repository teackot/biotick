#pragma once

#include <vector>
using std::vector;

#include "objects/Object.h"
using obj::Object;

class Field {
private:
    Object ***field; // Object *field[][]
    int x_size, y_size;

public:
    Field(int m, int n);
    ~Field();

    [[nodiscard]] int get_x_size() const;
    [[nodiscard]] int get_y_size() const;

    [[nodiscard]] Object *&at(int x, int y);

//    [[nodiscard]] Object *get(int x, int y) const;
//                  void    set(int x, int y, Object *o);
};
