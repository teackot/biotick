#include "Crab.h"

#include "Wall.h"

#include <random>
static int crab_rand() {
    static std::random_device rd;
    static std::mt19937 gen(rd());
    static std::uniform_int_distribution<> d(-1, 1);

    return d(gen);
}

static char directions[3][3] = {
        {'\\', '^', '/'},
        {'<',  'o', '>'},
        {'/',  'v', '\\'},
};

namespace obj {
    Crab::Crab(int x_rel, int y_rel) {
        this->x_rel = x_rel;
        this->y_rel = y_rel;
        this->timer = 0;
        this->object_to_spawn = nullptr;

        character = directions[y_rel + 1][x_rel + 1];
    }

    char Crab::as_char() const {
        return character;
    }

    Action Crab::tick() {
        timer++;
        if (timer == 15) {
            // multiply
            this->object_to_spawn = new Crab(crab_rand(), crab_rand());
            timer = 0;
            return {action::SPAWN, -1 * x_rel, -1 * y_rel};
        } else if (timer % 2 == 1) {
            return {action::HIT, x_rel, y_rel};
        } else {
            return {action::MOVE, x_rel, y_rel};
        }
    }

    bool Crab::respond_to_action(Action action) {
        if (action.type == action::HIT) {
            this->object_to_spawn = new Wall();
            return true;
        }

        return false;
    }

    Object *Crab::get_object_to_spawn() const {
        return object_to_spawn;
    }
} // obj
