#pragma once

#include "Object.h"

namespace obj {
    class Wall : public Object {
    public:
        [[nodiscard]] char as_char() const override;

        Action tick() override;

        bool respond_to_action(Action action) override;
    };
} // obj
