#pragma once

#include "../Action.h"

namespace obj {
    class Object {
    private:
        bool turn_flag = false;

    public:
        virtual ~Object() = default;

        [[nodiscard]] virtual char as_char() const;

        virtual Action tick() = 0;

        // true if submits to actor
        virtual bool respond_to_action(Action action);

        [[nodiscard]] bool get_turn_flag() const;
        void set_turn_flag(bool f);

        [[nodiscard]] virtual Object *get_object_to_spawn() const;
    };
} // obj
