#pragma once

#include "Object.h"

namespace obj {
    class Crab : public Object {
    private:
        char character;

        int x_rel, y_rel;

        int timer;

        Object *object_to_spawn;

    public:
        Crab(int x_rel, int y_rel);

        [[nodiscard]] char as_char() const override;

        Action tick() override;

        bool respond_to_action(Action action) override;

        [[nodiscard]] Object *get_object_to_spawn() const override;
    };
} // obj
