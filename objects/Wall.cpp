#include "Wall.h"

#include <iostream>

namespace obj {
    char Wall::as_char() const {
        return '#';
    }

    Action Wall::tick() {
        return {action::NOP};
    }

    bool Wall::respond_to_action(Action action) {
        if (action.type == action::HIT){
            return true;
        }
        return false;
    }
} // obj
