#include "Object.h"

namespace obj {
    bool Object::respond_to_action(Action action) { return false; }

    bool Object::get_turn_flag() const {
        return turn_flag;
    }

    void Object::set_turn_flag(bool f) {
        turn_flag = f;
    }

    Object *Object::get_object_to_spawn() const {
        return nullptr;
    }

    char Object::as_char() const {
        return 'o';
    }
} // obj
