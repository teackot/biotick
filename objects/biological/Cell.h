#pragma once

#include <utility>

#include "../Object.h"

class Direction {
private:
    static constexpr std::pair<int, int> offsets[8] = {
            {-1, -1}, // 0 1 2
            { 0, -1}, // 7 x 3
            { 1, -1}, // 6 5 4
            { 1,  0},
            { 1,  1},
            { 0,  1},
            {-1,  1},
            {-1,  0},
    };

    static int wrap(int dir);

    int direction;

public:
    Direction(int dir);

    Direction &operator=(int dir);

    void rotate(int angle);
    Direction &operator+=(int angle);
    Direction &operator-=(int angle);

    std::pair<int, int> operator[](int angle) const;
};

namespace obj::bio {
    class Cell : public Object {
    private:
        Direction dir;

        unsigned char *dna;
        int dna_size;

    public:
        Cell(const unsigned char *dna, int dna_size, int dir = -1);
        ~Cell() override;

        [[nodiscard]] char as_char() const override;

        Action tick() override;

        bool respond_to_action(Action action) override;
    };
} // obj::bio
