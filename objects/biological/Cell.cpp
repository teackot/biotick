#include "Cell.h"

Direction::Direction(int dir) {
    this->direction = wrap(dir);
}

int Direction::wrap(int dir) {
    return dir & (8 - 1);
}

Direction &Direction::operator=(int dir) {
    this->direction = wrap(dir);
    return *this;
}

void Direction::rotate(int angle) {
    direction = wrap(direction + angle);
}
Direction &Direction::operator+=(int angle) {
    rotate(angle);
    return *this;
}
Direction &Direction::operator-=(int angle) {
    rotate(-angle);
    return *this;
}

std::pair<int, int> Direction::operator[](int angle) const {
    return offsets[wrap(direction + angle)];
}

#include <random>
static int cell_rand() {
    static std::random_device rd;
    static std::mt19937 gen(rd());
    static std::uniform_int_distribution<> d(0, 7);

    return d(gen);
}

static char directions[3][3] = {
        {'F', '^', '7'},
        {'<',  'o', '>'},
        {'L',  'v', 'J'},
};

namespace obj::bio {
    Cell::Cell(const unsigned char *dna, int dna_size, int dir)
        : dir(dir == -1 ? cell_rand() : dir) {
        this->dna_size = dna_size;
        this->dna = new unsigned char [dna_size];
        for (int i = 0; i < dna_size; i++) {
            this->dna[i] = dna[i];
        }
    }

    Cell::~Cell() {
        delete[] dna;
    }

    char Cell::as_char() const {
        auto offsets = dir[0];
        return directions[1 + offsets.second][1 + offsets.first];
    }

    Action Cell::tick() {
        auto offsets = dir[0];
        return {action::MOVE, offsets.first, offsets.second};
    }

    bool Cell::respond_to_action(Action action) {
        return false;
    }
} // obj::bio
